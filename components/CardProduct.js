import Image from 'next/image';
import React from 'react';

export default function CardProduct({ thumb, title, desc, category }) {
  console.log(thumb);
  return (
    <div className='relative w-full max-w-full rounded-xl'>
      <div className='w-full border border-slate-400 p-4 rounded-xl mb-1'>
        <Image
          className='rounded-lg mx-auto'
          src={thumb}
          width={165}
          height={96}
          alt='logo'
        />
      </div>
      <h1 className='font-bold mb-1'>{title}</h1>
      <p className='h-12 overflow-hidden text-slate-700'>{desc}</p>
      <div className='flex justify-end mt-3'>
        <span className='inline-block px-4 py-1 bg-slate-200 rounded-full text-sm'>
          {category}
        </span>
      </div>
    </div>
  );
}
