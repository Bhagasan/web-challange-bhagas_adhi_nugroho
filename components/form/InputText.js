import React from 'react';

export default function InputText({
  placeholder,
  value,
  onChange,
  secret = false,
  fluid = false,
  ...props
}) {
  return (
    <div className='relative'>
      <input
        {...props}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        type={secret ? 'password' : 'text'}
        className={`border-b border-gray-400 peer outline-none h-8 placeholder:text-xs text-purple-900 ${
          fluid ? 'w-full' : ''
        }`}
      />
      <span className='absolute bottom-0 left-0 w-full h-px bg-purple-900 origin-left scale-x-0 peer-focus:scale-x-100 duration-300'></span>
    </div>
  );
}
