import React from 'react';

export default function Label({ text, htmlFor }) {
  return (
    <label className='text-sm block mb-1' htmlFor={htmlFor}>
      {text}
    </label>
  );
}
