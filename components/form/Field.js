import React from 'react';
import Label from './Label';

export default function Field({ children, label, htmlFor }) {
  return (
    <div className='relative my-6'>
      <Label text={label} htmlFor={htmlFor} />
      {children}
    </div>
  );
}
