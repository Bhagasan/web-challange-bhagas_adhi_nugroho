import Image from 'next/image';
import Link from 'next/link';
import React from 'react';

const MENU = [
  {
    label: 'Dashboard',
    path: '/dashboard',
  },
  {
    label: 'Products',
    path: '/products',
  },
];

export default function Sidebar() {
  const renderMenu = () => {
    const elm = [];
    MENU.forEach((d) => {
      elm.push(
        <li className='w-full hover:bg-purple-700 hover:text-white'>
          <Link className='block px-8 py-4' href={d.path}>
            {d.label}
          </Link>
        </li>
      );
    });
    return <ul className='block'>{elm}</ul>;
  };
  return (
    <div className='relative overflow-auto m-4 w-full bg-gray-200 rounded-2xl'>
      <div className='w-20 mx-auto mt-4 mb-12'>
        <Image src={'/images/logo.png'} width={165} height={96} alt='logo' />
      </div>
      {renderMenu()}
    </div>
  );
}
