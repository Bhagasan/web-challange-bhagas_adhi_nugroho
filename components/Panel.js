import React from 'react';

export default function Panel({ children }) {
  return (
    <div className='relative py-4 px-6 rounded-xl border-gray-200 border shadow-gray-500 bg-white'>
      {children}
    </div>
  );
}
