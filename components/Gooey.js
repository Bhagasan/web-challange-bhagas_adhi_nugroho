import React from 'react';

export default function Gooey() {
  return (
    <div className='absolute w-[360px] left-0 top-0'>
      <div className='wave'></div>
      <div className='wave'></div>
    </div>
  );
}
