import React from 'react';

export default function Modal({ children, onClose, isActive }) {
  return (
    <div
      className={`fixed h-screen w-screen left-0 top-0 bg-gray-900 bg-opacity-50 flex justify-center items-center z-50 duration-200 backdrop-blur-sm ${
        isActive
          ? 'opacity-100 delay-0 pointer-events-auto'
          : 'opacity-0 delay-150 pointer-events-none'
      }`}
    >
      <div
        className='absolute left-0 top-0 right-0 bottom-0'
        onClick={onClose}
      ></div>
      {children}
    </div>
  );
}
