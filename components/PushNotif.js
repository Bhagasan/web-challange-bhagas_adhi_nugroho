import React, { useEffect } from 'react';
import { DANGER, DEFAULT, SUCCESS, WARNING } from '../helper/constants';

export default function PushNotif({
  children,
  isActive = false,
  onClose,
  type = DEFAULT,
}) {
  useEffect(() => {
    const timeout = setTimeout(() => {
      onClose();
    }, 2000);
    return () => clearTimeout(timeout);
  }, [onClose, isActive]);

  const renderBg = () => {
    let color = 'bg-gray-500';
    switch (type) {
      case DANGER:
        color = 'bg-red-500';
        break;
      case SUCCESS:
        color = 'bg-red-500';
        break;
      case WARNING:
        color = 'bg-orange-500';
        break;
      default:
        break;
    }
    return color;
  };
  return (
    <div
      onClick={onClose}
      className={`fixed top-3 left-1/2 -translate-x-1/2 duration-300 cursor-default ${
        isActive
          ? 'opacity-100 delay-0 pointer-events-auto'
          : 'opacity-0 pointer-events-none delay-200'
      }`}
    >
      <div
        className={`relative py-4 px-6 rounded-xl border-gray-200 border shadow-gray-500 duration-300 ${renderBg()} text-white ${
          isActive ? 'translate-y-0 opacity-100' : '-translate-y-10 opacity-0'
        }`}
      >
        {children}
      </div>
    </div>
  );
}
