import React from 'react';

export default function ButtonFilled({ children, disabled, onClick }) {
  return (
    <button
      onClick={onClick}
      disabled={disabled}
      className={`px-3 py-2 min-w-[120px] font-bold rounded-full text-sm text-white hover:bg-purple-950 ${
        disabled ? 'bg-gray-400' : 'bg-purple-800'
      }`}
    >
      {children}
    </button>
  );
}
