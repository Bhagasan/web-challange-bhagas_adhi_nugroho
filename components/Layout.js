import React from 'react';
import Sidebar from './Sidebar';

export default function Layout({ children }) {
  return (
    <div className='relative flex min-h-screen'>
      <div className='w-[300px] md:block hidden'>
        <Sidebar />
      </div>
      <div className='md:w-[calc(100%-300px) w-full p-8 h-screen overflow-auto'>
        {children}
      </div>
    </div>
  );
}
