'use client';
import React, { useEffect, useState } from 'react';
import { getProducts } from '@/actions';
import Layout from '@/components/Layout';
import CardProduct from '@/components/CardProduct';

export default function Products() {
  const [listProduct, setListProduct] = useState([]);
  useEffect(() => {
    getProducts().then((res) => {
      setListProduct(res.products);
    });
  }, []);

  const renderCards = (params) => {
    const elm = [];
    params.forEach((d) => {
      elm.push(
        <CardProduct
          thumb={d.thumbnail}
          title={d.title}
          desc={d.description}
          category={d.category}
        />
      );
    });
    return elm;
  };

  console.log(listProduct);
  return (
    <Layout>
      <div className='relative w-full grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 grid-cols-1 md:gap-6 gap-3'>
        {renderCards(listProduct)}
      </div>
    </Layout>
  );
}
