'use client';
import { useState } from 'react';
import Image from 'next/image';
import Field from '@/components/form/Field';
import InputText from '@/components/form/InputText';
import ButtonFilled from '@/components/ButtonFilled';
import Link from 'next/link';
import PushNotif from '@/components/PushNotif';
import { DANGER, DEFAULT } from '@/helper/constants';
import { login } from '@/actions';
import { useRouter } from 'next/navigation';
import Gooey from '@/components/Gooey';

export default function Home() {
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const [dataPush, setDataPush] = useState({
    type: DEFAULT,
    msg: '',
    shown: false,
  });
  const [payload, setPayload] = useState({
    username: '',
    password: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setPayload((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const anyEmpty = () => {
    let temp = false;
    Object.values(payload).forEach((d) => {
      if (!d) temp = true;
    });
    return temp;
  };

  const handleKeyUp = (e) => {
    const { code } = e;
    if (code === 'Enter') {
      handleSubmit();
    }
  };

  const handleSubmit = () => {
    const { username, password } = payload;
    if (!anyEmpty()) {
      setIsLoading(true);
      login(username, password)
        .then((res) => {
          console.log(res);
          setIsLoading(false);
          router.push('dashboard');
        })
        .catch(() => {
          setIsLoading(false);
          setDataPush({
            type: DANGER,
            msg: 'Username atau Password salah.',
            shown: true,
          });
        });
    } else {
      setDataPush({
        type: DANGER,
        msg: 'User ID dan atau Password anda belum diisi.',
        shown: true,
      });
    }
  };
  return (
    <main className='flex min-h-screen items-center justify-center p-4 overflow-hidden'>
      <Gooey />
      <div className='w-[320px] px-4'>
        <div className='mb-5'>
          <div className='block mx-auto mb-20 w-28'>
            <Image
              src={'/images/logo.png'}
              width={165}
              height={96}
              alt='logo'
            />
          </div>
          <h1 className='font-bold text-xl mb-2'>Login</h1>
          <p className='text-sm'>Please sign in to continue</p>
        </div>
        <div>
          <Field label='User ID' htmlFor='username'>
            <InputText
              name='username'
              onChange={handleChange}
              onKeyUp={handleKeyUp}
              fluid
              id='username'
              placeholder='User ID'
            />
          </Field>
          <Field label='Password' htmlFor='password'>
            <InputText
              name='password'
              onChange={handleChange}
              onKeyUp={handleKeyUp}
              fluid
              id='password'
              placeholder='Password'
              secret
            />
          </Field>
          <div className='flex justify-end'>
            {isLoading ? (
              <div className='w-28 h-9 flex justify-center'>
                <Image
                  src='/images/loading.svg'
                  width={32}
                  height={32}
                  alt='loading'
                />
              </div>
            ) : (
              <ButtonFilled onClick={handleSubmit}>LOGIN</ButtonFilled>
            )}
          </div>
        </div>
        <div className='text-center text-sm mt-20'>
          <span className='text-gray-600'>Don`t have an account?</span>
          <Link href='/' className='text-red-600 inline-block ml-2'>
            Sign Up
          </Link>
        </div>
      </div>
      <PushNotif
        type={dataPush.type}
        isActive={dataPush.shown}
        onClose={() =>
          setDataPush((prev) => ({
            ...prev,
            shown: false,
          }))
        }
      >
        <p>{dataPush.msg}</p>
      </PushNotif>
    </main>
  );
}
