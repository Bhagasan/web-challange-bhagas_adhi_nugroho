export async function login(username, password) {
  const res = await fetch('https://dummyjson.com/auth/login', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      username,
      password,
    }),
  });

  if (!res.ok) throw new Error('Failed to Login');
  return res.json();
}

export async function getProducts() {
  const res = await fetch('https://dummyjson.com/products');
  if (!res.ok) throw new Error('Failed to get products');
  return res.json();
}

// username: 'kminchelle',
//       password: '0lelplR',
